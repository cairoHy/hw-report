\section{实验步骤}

\subsection{模型介绍}

本次实验的模型前文已经提到过，该模型属于神经网络模型，主要利用到的现有研究成果有循环神经网络\upcite{Deutsch2012Supervised}和注意力机制\upcite{Bahdanau2014Neural}。

\subsubsection{循环神经网络}

如图\ref{fig:rnn-structure}所示，循环神经网络(Recurrent Neural Network,RNN)是一种在隐藏层单元中引入层内连接的神经网络，同前馈神经网络相比，在处理输入之间相互关联的序列时表现更好。例如，在一个语言模型中，你要预测句子的下一个单词是什么，就需要用到前面的单词，因为一个句子中前后单词是相关的。

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{imgs/2/RNN-structure}
	\caption[经典RNN结构]{经典RNN结构}
	\label{fig:rnn-structure}
\end{figure}

一般我们使用如图\ref{fig:rnn-structure-unfold}所示的展开结构来解释RNN的工作原理，该网络包含输入层和隐藏层两层，其中
\begin{itemize}
	\item 假定输入层有i个神经元，隐藏层有h个神经元，输出层有o个神经元，网络的时间步长度为T。
	\item 输入为$\{\vec{x_1},\vec{x_2},...,\vec{x_T}\}$，其中$\vec{x_t}\in R^{i\times 1}$。
	\item 输出为$\{\vec{o_1},\vec{o_2},...,\vec{o_T}\}$。其中，$\vec{o_t} = softmax(\vec{V}\vec{s_t}),\vec{o_t}\in R^{o\times 1}$。
	\item 隐藏层输出$\{\vec{s_1},\vec{s_2},...,\vec{s_T}\}$，也被称为隐层状态。其中$\vec{s_t}=f(\vec{U}\vec{x_t} + \vec{W}\vec{s_{t-1}}),\vec{s_t}\in R^{h \times 1}$，也就是t时刻的隐层状态由t-1时刻的隐层状态和t时刻输入共同决定，其中f是一个非线性激活函数。
	\item 经典的RNN网络有三个参数矩阵，$U\in R^{h\times i},W\in R^{h\times h},V\in R^{o\times h}$，一般使用BPTT(Back Propagation Through Time)算法\upcite{Werbos1990Backpropagation}进行参数更新。其思想主要是由于隐层神经元激活值不仅影响当前时刻输出，而且影响下一时刻隐层单元的值，因此误差的反向传播也要包含从输出层到隐层以及从上一时刻隐层到这一时刻隐层这两条路径。
	\item RNN网络的正向传播就是从1到T时刻，计算每个时刻的输出和隐层状态；基于BPTT算法的反向传播则是从T到1时刻，将误差对参数的偏导数反向计算出来。
	\item 在自然语言处理任务中应用RNN时，一般将一个单词的向量表示$\{i_1,i_2,...,i_n\}$作为一个时刻的输入$\vec{x_t}$，而整个单词序列（句子或者段落）就是RNN从1到T时刻的输入。
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{imgs/2/RNN-structure-unfold}
	\caption[经典RNN展开结构]{经典RNN展开结构}
	\label{fig:rnn-structure-unfold}
\end{figure}

经典的RNN网络在应用中会出现梯度消失的问题，为了解决这个问题，RNN网络的很多变种被提出来，包括长短时记忆网络(Long short Time Memory,LSTM)和GRU(Gated Recurrent Unit)\upcite{Chung2014Empirical}等，在RNN的基础上设计更加精密，原理在此暂不赘述，本文的模型就使用了GRU网络。

\subsubsection{注意力机制}

在人类认知一个语言序列或者图像的时候，总是根据目标的不同对其中某一部分更加留意。注意力机制就是根据这个认知神经学的原理，被应用在视觉和自然语言领域。

注意力机制最早在2014年被使用在自然语言处理领域，当时的一篇论文《Neural Machine Translation by Jointly Learning to Align and Translate》利用注意力机制和RNN设计了一个神经翻译模型，此后，基于各种注意力机制的研究层出不穷。

在此，我们使用最早的这篇论文来简述一下注意力机制的原理。如图\ref{fig:seq2seq-attention}所示，是该论文提出的翻译模型，其中encoder和decoder分别是两个RNN。但是decoder中隐层状态的计算公式变成了$s_i=f(s_{i-1},o_{i-1},c_i)$，而这个多出来的上下文向量$c_i$，就是使用注意力机制计算出来的encoder每个时刻隐层状态$h_j$的加权平均：$c_i=\sum_j\alpha_{ij}h_j$。

\begin{figure}
	\centering
	\includegraphics[width=0.34\linewidth]{imgs/2/seq2seq-attention}
	\caption[seq2seq模型中的注意力机制]{seq2seq模型中的注意力机制}
	\label{fig:seq2seq-attention}
\end{figure}

上面公式中的$\alpha_{ij}$是归一化后的注意力权重：$\alpha_{ij}=\frac{exp(e_{ij)}}{\sum_kexp(e_{ik})}$。原始的注意力权重$e_{ij}$是通过比较decoder上一时刻的隐层状态$s_{i-1}$和encoder的每个时刻隐层状态$h_j$计算出来的，即$e_{ij}=sim(s_{i-1},h_j)$，其中$sim$是向量相似度计算函数，在每个模型中的实现各不相同。这也符合我们对注意力机制的理解，decoder上一时刻的隐层状态代表现在即将翻译的单词的上下文，而它和encoder的哪个隐层状态相似，也就说明我们翻译当前单词的时候，需要向源语句中的哪个部分投入注意力。

当然，后来的模型中，逐渐发展出各种各样的注意力机制，但是这些机制的基本思想都是同上面所述类似的，只是根据使用的场景做了调整和优化。

\subsubsection{Attention-Sum-Reader}

下面，终于讲到了本文实现的模型，Attention-Sum-Reader（简称AS-Reader）了。它是一个面向完形填空任务的模型。模型的结构如图\ref{fig:as-reader}所示：
\begin{itemize}
	\item 1.通过一层词向量矩阵$e$将上下文文档$d$和问题$q$中one-hot表示的单词分别映射成embedding向量表示。
	\item 2.用一个单层双向GRU来编码上下文文档$d$，得到$d$中每个单词的表示，用每个时刻的隐层输出表示当前时刻的单词，则可以抽象的理解为$d$被通过一个有参数的映射$f$编码为$f_d$。
	\item 3.用一个单层双向GRU来编码问题$q$，用最后时刻的隐层输出来表示$q$，可以抽象的理解为$q$被通过一个有参数的映射$g$编码为$g_q$。
	\item 4.$d$中每个单词的表示与$q$的表示作点积后，进行归一化，其结果作为注意力权重$s_i$，即$q$与$d$中的每个词$i$之间的相关性度量。
	\item 5.对候选单词集合$A$中的每个单词，将其在$d$中每次出现$i$的概率$s_i$相加，得到每个候选词的概率，概率最大的词$w$即为预测的答案，也就是条件概率$P(a=w|q,d,\theta)$。
	\item 6.在训练的时候，对$e,f,g$这三个部分的参数进行联合训练，以$-\log P_{\theta}(a|q,d)$作为目标函数，使用Adam作为优化算法。
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=1.0\linewidth]{imgs/2/as-reader}
	\caption[Attention-Sum-Reader]{Attention-Sum-Reader}
	\label{fig:as-reader}
\end{figure}

\subsection{数据集分析和数据预处理}

\subsubsection{数据集}
综合数据量和机器性能两方面考量，我选择了Facebook在2015年提出的一个完形填空数据集CBT(The Children's Book Test)作为目标数据集，这个数据集包含90000余条训练数据、6000条验证数据和2500条测试数据，数据量大小适中。这个数据集使用儿童故事的前二十句作为上下文的文档，用第二十一句挖空某个单词作为问题，并将挖空的单词和其他随机挑选的九个相同词性的词放在一起作为候选答案。根据挖去词的词性不同，该数据集分为四个子数据集分别是命名实体(Name Entity,NE)、通用名词(Common Nouns,CN)、介词(Prepositions,P)和动词(Verbs, V)。本次实验由于训练时间较久，我主要在CBT-NE数据集上进行实验。

\subsubsection{预处理}
由于模型的输入是Python中numpy库的数组格式，因此需要对数据进行预处理，我的预处理步骤主要有以下步骤，这些步骤主要写在\textit{data\_utils.py}源码中：
\begin{itemize}
	\item 按格式读取数据集，进行词频统计、分词，构建词库。
	\item 使用预训练的Glove词向量，同构建的词库一起，建立初始的词向量矩阵。
	\item 将数据文件ID化，加快再次读取时的速度。
	\item 将ID化的数据文件读取为Numpy数组，并进行shuffle操作。
\end{itemize}

\subsection{模型实现}

在Keras和TensorFlow两个框架下分别实现了模型，分别对应源码中\textit{attention\_sum\_reader.py}和\textit{as\_reader\_tf.py}两个文件，可以在运行时设置参数"--framework tensorflow"或者"--framework keras"指定。

下面对模型实现中遇到的典型问题及解决方案进行简要描述\cite{zhanghaoyu1993}：

问题一：这个模型单纯使用Keras能够实现吗？

解答：无法实现，因为Keras只对常见的、经典的几种层进行了抽象形成了API，而本模型中涉及到的一些对张量(Tensor)的操作并没有提供，需要调用keras.backend中的方法进行实现，甚至需要直接调用TensorFlow中张量操作的方法。

问题二：在使用RNN的时候怎样处理变长的序列？

解答：因为RNN实际上还是接受定长输入的，所以在实际实现中有两种解决方案：1.将序列长度用0补足为最长的序列并送入RNN中处理，如果使用TensorFlow，那么可以使用dynamic\_rnn()方法，把序列的实际长度列表作为其中一个输入参数，只会处理实际的序列长度，能够减少浪费在pad字符上的处理时间；2.将长度相差较多的数据分为不同的bucket，将某个长度范围内的序列放在一个bucket里面，用一个模型处理，Google的Seq2Seq模型使用了这种做法。这种做法像是某种集成模型，其缺点是会减少训练数据量。本文采取了第一种方案。

问题三：TensorFlow和Keras的比较？

解答：Keras写起来更加简便快捷，而且和底层的TensorFlow无缝衔接，但是有一个缺点就是同样的模型运行速度慢很多、GPU消耗高很多，抽象级别太高，导致失去了一定的灵活性。而TensorFlow写起来更加冗长一些，但在你需要运行速度和精细的张量操作的时候是必需的。